import base64
import binascii
import random
import pytest
import os
import sys
import uncurl

import unittest

import requests
from flask import Flask, current_app, jsonify, request
from requests import auth

import demo_app
from demo_app import api as flask_app
from demo_app.auth import login
from demo_app.db import get_db
from testing.api.support import account_data_provider
from testing.api.config.environment_constants import get_formatted_URL
from testing.api.config.endpoints import Endpoints
from  demo_app.api import login_required
path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(path)


def perform_token_call():

    return requests.get(get_formatted_endpoint_url(Endpoints.AUTHENTICATION),
                        auth=(account_data_provider.get_account_username(expectedUserData),
                              account_data_provider.get_account_password(expectedUserData)))


def get_token():
    token_response = perform_token_call()

    if token_response.status_code >= 400:
        raise Exception('Response call returned status code: {}'.format(token_response.status_code))
    else:
        return token_response.json()["token"]


def get_formatted_endpoint_url(endpoint):
    return "{}{}".format(get_formatted_URL(), endpoint)


def getUserResponse(endpoint):
    return requests.get(get_formatted_endpoint_url(endpoint),
                        headers={'Token': '{}'.format(get_token()),
                                 'Content-Type': 'application/json',
                                 'Accept': 'application/json'})

def generate_key():
    return str(base64.b64encode(str(random.getrandbits(128)).encode()), "utf-8")







expectedUserData = 'EXISTING_USER_A'
expectedUserData1 = 'INVALID_USER_ENDPOINT'
EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData)
NON_EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData1)



class TestUnit(unittest.TestCase):
    @pytest.fixture
    def app(self):
        yield flask_app

    @pytest.fixture
    def client(demo_app):
        return demo_app.test_client()


    def test_login_success(self):
        user = 'EXISTING_USER_A'
        username = account_data_provider.get_account_username(user)
        password = account_data_provider.get_account_username(user)
        myHeaders = {'Token': '{}'.format(get_token()), 'Content-Type': 'application/json',
                     'Accept': 'application/json'}
        url = get_formatted_endpoint_url(Endpoints.AUTHENTICATION + '/' + Endpoints.LOGIN)
        status = uncurl.parse("curl 'api_user:Test@1234!''http://0.0.0.0:5000/api/auth/token'")
        print(url)
        #token = create_token()
        print(status)





    def test_get_locations_for_us_90209_check_status_code_equals_200(self):
        """ Note this is testing purpose not in the requrirement to check weather setup is working or not """
        response = requests.get("http://api.zippopotam.us/us/90209")
        assert response.status_code == 200


    def test_test(self):

        demo_app.api.create_token()
        print(auth)
        """ Testing direct urls weather this really works or not """
        myHeaders = {'Token': '{}'.format(get_token()), 'Content-Type': 'application/json',
                     'Accept': 'application/json'}

        url = 'http://0.0.0.0:5000/api/token/login'
        proxy = {'http': 'http://0.0.0.0:50000/api/token/login'}
        response = requests.post(url, auth=('api_user', 'Test@1233!'), headers=myHeaders)
        # print request object
        print(response.headers)
        print(response.text)
        print(requests.auth)

    def test_token_endpoint(self):
        try:
            response = perform_token_call()
            self.assertEqual("application/json", response.headers['Content-Type'])
            self.assertTrue(response.ok)
            self.assertTrue(200, response.status_code)
            self.assertEqual('SUCCESS', response.json()['status'])
            self.assertEqual(52, len(response.json()['token']))
            self.assertTrue(response.json()['token'].isalnum())
            # print(response.json()['token'])
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())

    def test_responseHeader(self):
        try:
            response = getUserResponse(EXISTING_USER_ENDPOINT)

            self.assertEqual('application/json', response.headers['Content-Type'])
            self.assertEqual('Werkzeug/1.0.1 Python/3.7.5', response.headers['Server'])
            self.assertEqual(123, int(response.headers['Content-Length']))
            # print(response.json)
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())

    def test_positive_get_user_data(self):
        try:
            response = getUserResponse(EXISTING_USER_ENDPOINT)

            self.assertTrue(response.ok)
            self.assertEqual('SUCCESS', response.json()['status'])
            self.assertEqual("retrieval succesful", response.json()['message'])

            self.assertEqual(account_data_provider.get_user_attribute_value(expectedUserData, 'firstname'),
                             response.json()['payload']['firstname'])
            self.assertEqual(account_data_provider.get_user_attribute_value(expectedUserData, 'lastname'),
                             response.json()['payload']['lastname'])
            self.assertEqual(account_data_provider.get_user_attribute_value(expectedUserData, 'phone'),
                             response.json()['payload']['phone'])
            # print(response.json()['message'])
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())

    def test_negative_get_user_data(self):
        try:
            response = getUserResponse(NON_EXISTING_USER_ENDPOINT)

            self.assertTrue(response.ok)
            self.assertEqual('SUCCESS', response.json()['status'])
            self.assertEqual("retrieval succesful", response.json()['message'])

            self.assertEqual(account_data_provider.get_user_attribute_value(expectedUserData, 'firstname'),
                             response.json()['payload']['firstname'])
            self.assertEqual(account_data_provider.get_user_attribute_value(expectedUserData, 'lastname'),
                             response.json()['payload']['lastname'])
            self.assertEqual(account_data_provider.get_user_attribute_value(expectedUserData, 'phone'),
                             response.json()['payload']['phone'])
            # print(response.json()['message'])
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())

    def test_negative_get_user_data_404(self):
        try:
            response = getUserResponse('INVALID_USER_ENDPOINT')
            self.assertFalse(response.ok)
            self.assertEqual(404, int(response.status_code))
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())


    def test_negative_get_user_data_500(self):
        try:
            response = getUserResponse(NON_EXISTING_USER_ENDPOINT)
            self.assertFalse(response.ok)
            self.assertEqual(500, int(response.status_code))
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())

    def test_get_users_list(self):
        try:
            response = getUserResponse(Endpoints.USERS)
            self.assertTrue(response.ok)
            self.assertEqual(200, response.status_code)

            users = response.json()['payload']
            expected_username = account_data_provider.get_account_username(expectedUserData)
            if expected_username not in users:
                self.fail(expected_username + ' could not be found within the users list!')

        except requests.exceptions.HTTPError as error_http:
            print(error_http)

        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)
            print(response.raise_for_status())

    def test_put_user(self):
        try:
            user = 'EXISTING_USER_A'
            # password = generate_password_hash('Test1234!')
            # print(password)
            myHeaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json'
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                json=user)
            print(response.content)

            self.assertTrue(response.ok)
            self.assertEqual(201, response.status_code)

            createResponseCheck = getUserResponse(user)

            self.assertEqual(200, createResponseCheck.status_code)
            self.assertEqual(account_data_provider.get_account_username(user),
                             createResponseCheck.json()['payload']['username'],
                             'Response First name was not the appropriate one')
            self.assertEqual(account_data_provider.get_account_first_name(user),
                             createResponseCheck.json()['payload']['firstname'])
            self.assertEqual(account_data_provider.get_account_last_name(user),
                             createResponseCheck.json()['payload']['lastname'])
            self.assertEqual(account_data_provider.get_account_phone(user),
                             createResponseCheck.json()['payload']['phone'])
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            #print(response.raise_for_status())


    def test_get_user_with_all_check_status_code_equals_200(self):
        try:
            user = 'EXISTING_USER_A'
            myHeaders = {'Token': '{MzQ2ODMxODU3MjYyMzExMjg5NzgyODk0MzUwMDAwMjg0OTA3Njg=}',
                         'Content-Type': 'application/json', 'Accept': 'application/json'
                         }
            # headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            response = requests.get(
                get_formatted_endpoint_url(Endpoints.AUTHENTICATION + '/' + Endpoints.USERS + account_data_provider.get_account_username(user)),
                json=user,
                headers=myHeaders)
            # response = requests.get("http://localhost:8180/api/users")
            assert response.status_code == 404

            print(response.json())
            print(getUserResponse(response.status_code))
            # print(response.history)

        # print(getUserResponse(user))
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
            print(response.raise_for_status())

    def test_get_user_with_all_check_status_404(self):
        try:
            user = 'EXISTING_USER_A'
            myHeaders = {'Token': '{MzQ2ODMxODU3MjYyMzExMjg5NzgyODk0MzUwMDAwMjg0OTA3Njg=}',
                         'Content-Type': 'application/json', 'Accept': 'application/json'
                         }
            # headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            #print(requests.get(get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            response = requests.get(
                get_formatted_endpoint_url(Endpoints.AUTHENTICATION + '/' + Endpoints.USERS),
                json=user,
                headers=myHeaders, timeout=5)
            print(getUserResponse(response))
            # response = requests.get("http://localhost:8180/api/auth/token/users")
            assert response.status_code == 200
            # print(response.json())
            # print(response.raise_for_status())

            # Code here will only run if the request is successful
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_update_user_bad_request(self):
        try:
            print("Planning to update user's info")
            user='EXISTING_USER_B'

            myHeaders = {'Token': '{}'.format(get_token()),
                     'Content-Type': 'application/json',
                     'Accept': 'application/json'
                     }


            r = requests.put('http://0.0.0.0:8181/api/users/api.user', user, headers=myHeaders)
            print(r.text)


        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)


    def test_login_required(self):
        user='EXISTING_USER_A'
        myHeaders = {'Token': '{}'.format(get_token()),
                     'Content-Type': 'application/json',
                     'Accept': 'application/json'
                     }
        #payload = {'user_name': 'api_user', 'password': 'Test@1234!'}
        # username = account_data_provider.get_account_username(user)
        # password = account_data_provider.get_account_password(user)
        # auth = {str(username),str(password)}
        r = requests.get('http://0.0.0.0:5000/login',user)
        print(r.url)
        print(r.text)
        print(login)

    def test_put_user(self):

        user = 'NEW_USER'

        myHeaders = {'Token': '{}'.format(get_token()),
                     'Content-Type': 'application/json',
                     'Accept': 'application/json'
                     }
        payload = {'firstname': 'Tom', 'lastname': 'Cruise'}
        r = requests.post('http://0.0.0.0:5000/users/api_user', payload,myHeaders)
        print(r.text)






