import base64
import binascii
import random
import pytest
import os
import sys
import uncurl

import unittest

import requests

from testing.api.support import account_data_provider
from testing.api.config.environment_constants import get_formatted_URL
from testing.api.config.endpoints import Endpoints
from demo_app.api import login_required


path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(path)


def perform_token_call():
    """ this methods retrieve the response when endpoint triggers for authentication
    """
    return requests.get(get_formatted_endpoint_url(Endpoints.AUTHENTICATION),
                        auth=(account_data_provider.get_account_username(expectedUserData),
                              account_data_provider.get_account_password(expectedUserData)))


def get_token():
    token_response = perform_token_call()

    if token_response.status_code >= 400:
        raise Exception('Response call returned status code: {}'.format(token_response.status_code))
    else:
        return token_response.json()["token"]


def get_text():
    token_text = perform_token_call()


def get_formatted_endpoint_url(endpoint):
    return "{}{}".format(get_formatted_URL(), endpoint)


def getUserResponse_get(endpoint):
    return requests.get(get_formatted_endpoint_url(endpoint),
                        headers={'Token': '{}'.format(get_token()),
                                 'Content-Type': 'application/json',
                                 'Accept': 'application/json'})
def getUserResponse_post(endpoint):
    return requests.post(get_formatted_endpoint_url(endpoint),
                        headers={'Token': '{}'.format(get_token()),
                                 'Content-Type': 'application/json',
                                 'Accept': 'application/json'})



def generate_key():
    """ how to generate the key"""
    return str(base64.b64encode(str(random.getrandbits(128)).encode()), "utf-8")


""" the data is testing purpose retrieving from json file"""
expectedUserData = 'EXISTING_USER_A'
expectedUserData1 = 'INVALID_USER_ENDPOINT'
EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData)
NON_EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData1)


class LoginUnitTest(unittest.TestCase):
    def test_login_success(self):
        """ this test validates login using authentication token """
        try:
           print(f"Start test Login Success Case")
           response_token = perform_token_call()
           print(f"Resoponse from Token: {response_token}")
           user = 'EXISTING_USER_A'
           username = account_data_provider.get_account_username(user)
           password = account_data_provider.get_account_username(user)
           myHeaders = {'Authorization': 'token {}'.format(get_token()),
                     'Content-Type': 'application/json', 'Accept': 'application/json'
                     }
           #r = requests.get('http://0.0.0.0:5000/api/token/login',myHeaders)
           #print(f"Request URL:{get_formatted_endpoint_url(Endpoints.AUTHENTICATION + '/' + Endpoints.LOGIN)}")
           response = requests.get(get_formatted_endpoint_url(Endpoints.LOGIN),
                                json=user,
                                headers=myHeaders)
           response1 = getUserResponse_post(Endpoints.LOGIN,user)
           print(response1.status_code)
           print(response1.url)
           #print(requests.status_codes)
           print(f"Respnse from Request{response}")
           result = response1.text
           print(result)
           print(f"End test Login Success Case")
           print(response.status_code)
        except requests.exceptions.HTTPError as errh:
           print(f"HTTP Error: {errh}")
        except requests.exceptions.ConnectionError as errc:
           print(f"Connection Error: {errc}")
        except requests.exceptions.Timeout as errt:
           print(f"Timeout Erro: {errt}")
        except requests.exceptions.RequestException as err:
           print(f"Request Exception: {err}")


