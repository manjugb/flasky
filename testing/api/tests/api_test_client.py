import base64
import json
import os
import random
import sys

import requests
from flask.json import jsonify

from demo_app.db import get_db
from testing.api.config.endpoints import Endpoints
from testing.api.config.environment_constants import get_formatted_URL, get_formatted_URL_login
from testing.api.support import account_data_provider

path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(path)

""" the data is testing purpose retrieving from json file"""
# user Data
expectedUserData = 'EXISTING_USER_A'
expectedUserData1 = 'NOT_EXIST_USER'
invalidData = 'EMPY_USER_C'
# user endpoints
EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData)
NON_EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData1)
NON_EXISTING_USER_ENDPOINT1 = Endpoints.USERS + '/' + account_data_provider.get_account_username(invalidData)



def perform_token_call():
    """ this methods retrieve the response when endpoint triggers for authentication
    """
    return requests.get(get_formatted_endpoint_url(Endpoints.AUTHENTICATION),
                 auth=(account_data_provider.get_account_username(expectedUserData),
                       account_data_provider.get_account_password(expectedUserData)))


def get_token():
    """ this function retrieves token if not fail"""
    token_response = perform_token_call()
    print(f"From token:{token_response.status_code}")
    print(token_response.json())

    if token_response.status_code >= 400:
        raise Exception('Response call returned status code: {}'.format(token_response.status_code))
    else:
        return token_response.json()["token"]

def get_formatted_endpoint_url(endpoint):
    return "{}{}".format(get_formatted_URL(), endpoint)

def get_formatted_endpoint_login_url(endpoint):
    return "{}{}".format(get_formatted_URL_login(),endpoint)


def getUserResponse(endpoint):
    return requests.get(get_formatted_endpoint_url(endpoint),
                        headers={'Token': '{}'.format(get_token()),
                                 'Content-Type': 'application/json',
                                 'Accept': 'application/json'})


