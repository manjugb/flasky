import base64
import json
import os
import random
import sys
import unittest
import pytest
import requests
from flask import request, jsonify
from simplejson import JSONDecodeError
from werkzeug.security import generate_password_hash

from demo_app.db import get_db
from testing.api.config.endpoints import Endpoints
from testing.api.config.environment_constants import get_formatted_URL
from testing.api.support import account_data_provider

path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(path)


""" the data is testing purpose retrieving from json file"""
expectedUserData = 'EXISTING_USER_A'
expectedUserData1 = 'INVALID_USER_ENDPOINT'
EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData)
NON_EXISTING_USER_ENDPOINT = Endpoints.USERS + '/' + account_data_provider.get_account_username(expectedUserData1)






def perform_token_call():
    """ this methods retrieve the response when endpoint triggers for authentication
    """
    return requests.get(get_formatted_endpoint_url(Endpoints.AUTHENTICATION),
                        auth=(account_data_provider.get_account_username(expectedUserData),
                              account_data_provider.get_account_password(expectedUserData)))


def get_token():
    token_response = perform_token_call()

    if token_response.status_code >= 400:
        raise Exception('Response call returned status code: {}'.format(token_response.status_code))
    else:
        return token_response.json()["token"]


def get_text():
    token_text = perform_token_call()


def get_formatted_endpoint_url(endpoint):
    return "{}{}".format(get_formatted_URL(), endpoint)


def getUserResponse(endpoint):
    return requests.get(get_formatted_endpoint_url(endpoint),
                        headers={'Token': '{}'.format(get_token()),
                                 'Content-Type': 'application/json',
                                 'Accept': 'application/json'})


def generate_key():
    """ how to generate the key"""
    return str(base64.b64encode(str(random.getrandbits(128)).encode()), "utf-8")

class TestUser(unittest.TestCase):
    def test_responseHeader(self):
        """ this test validate response header"""
        try:
            response = getUserResponse(EXISTING_USER_ENDPOINT)
            self.assertEqual('application/json', response.headers['Content-Type'])
            self.assertEqual('Werkzeug/1.0.1 Python/3.7.5', response.headers['Server'])
            self.assertEqual(124, int(response.headers['Content-Length']))
            # print(response.json)
        except requests.exceptions.HTTPError as errh:
            print(f"Http Error: {errh}")
        except requests.exceptions.ConnectionError as errc:
            print(f"Http Connection Error: {errc}")
        except requests.exceptions.Timeout as errt:
            print(f"Time Out: {errt}")
        except requests.exceptions.RequestException as err:
            print(f"Request Exception: {err}")
            print(response.raise_for_status())

    def test_get_users_success(self):
        """ this test case validates to list the users"""
        try:
            response = getUserResponse(Endpoints.USERS)
            self.assertTrue(response.ok)
            self.assertEqual(response.status_code,200)
            print(response.text)
            users = response.json()['payload']
            expected_username = account_data_provider.get_account_username(expectedUserData)
            if expected_username not in users:
               self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(f"Connection Error:{error_connect}")
        except requests.exceptions.Timeout as error_timeout:
            print(f"Timeout:{error_timeout}")
        except requests.exceptions.RequestException as error:
            print(f"Request Exception:{error}")
            print(response.raise_for_status())

    def test_get_users_failure_404(self):
        """ this test case validates to not to list the
        users and get 404 status code"""
        try:
            response = getUserResponse(Endpoints.USERS + '/')
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code,404)
            print(response.text)
           # users = response.json()['payload']
           # expected_username = account_data_provider.get_account_username(expectedUserData)
           # if expected_username not in users:
            #   self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(f"Connection Error:{error_connect}")
        except requests.exceptions.Timeout as error_timeout:
            print(f"Timeout:{error_timeout}")
        except requests.exceptions.RequestException as error:
            print(f"Request Exception:{error}")
            print(response.raise_for_status())

    def test_get_ussers_status_code_500(self):
        """this test validates invalid end point and gives that 500 error"""
        """ this test case validates to list the users"""
        try:
            response = getUserResponse(Endpoints.USERS + '/userlist')
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code, 500)
            print(response.text)
            # users = response.json()['payload']
            # expected_username = account_data_provider.get_account_username(expectedUserData)
            # if expected_username not in users:
            #     self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(f"Connection Error:{error_connect}")
        except requests.exceptions.Timeout as error_timeout:
            print(f"Timeout:{error_timeout}")
        except requests.exceptions.RequestException as error:
            print(f"Request Exception:{error}")
            print(response.raise_for_status())

    def test_get_users_status_code_404(self):
        """ this test case validates to list the users  """
        try:
            response = getUserResponse(Endpoints.USERS + Endpoints.LOGIN)
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code, 404)
            print(response.text)
            #users = response.json()['payload']
            # expected_username = account_data_provider.get_account_username(expectedUserData)
            # if expected_username not in users:
            #     self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(f"Connection Error:{error_connect}")
        except requests.exceptions.Timeout as error_timeout:
            print(f"Timeout:{error_timeout}")
        except requests.exceptions.RequestException as error:
            print(f"Request Exception:{error}")
            print(response.raise_for_status())

    def test_get_user_success_200(self):
        """ this test validates able to get single user
        verify user information """
        try:
           user='EXISTING_USER_A'
           response = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
           self.assertTrue(response.ok)
           self.assertEqual(response.status_code, 200)
           print(response.text)
           print(response.url)
           print(response.history)


           self.assertEqual(account_data_provider.get_account_first_name(user),
                            response.json()['payload']['firstname'])
           self.assertEqual(account_data_provider.get_account_last_name(user),
                            response.json()['payload']['lastname'])
           self.assertEqual(account_data_provider.get_account_phone(user),
                            response.json()['payload']['phone'])

        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(f"Connection Error:{error_connect}")
        except requests.exceptions.Timeout as error_timeout:
            print(f"Timeout:{error_timeout}")
        except requests.exceptions.RequestException as error:
            print(f"Request Exception:{error}")
            print(response.raise_for_status())

    def test_get_user_failure_500(self):
        """ this test validates user which is not exist
        and gives valid error message and check runtime exceptions """
        try:
            user = 'NOT_EXIST_USER'
            exp_url = 'http://0.0.0.0:5000/api/users/api_user3'
            response = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code, 500)
            self.assertEqual(response.url,exp_url)
            print(response.json)
            #print(response.url)
            #print(response.history)
            self.assertEqual(account_data_provider.get_account_first_name(user),
                             response.json()['payload']['firstname'])
            self.assertEqual(account_data_provider.get_account_last_name(user),
                             response.json()['payload']['lastname'])
            self.assertEqual(account_data_provider.get_account_phone(user),
                             response.json()['payload']['phone'])

        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(f"Connection Error:{error_connect}")
        except requests.exceptions.Timeout as error_timeout:
            print(f"Timeout:{error_timeout}")
        except requests.exceptions.RequestException as error:
            print(f"Request Exception:{error}")
        except JSONDecodeError as js_code_er:
            print(f"Json Code Error:{js_code_er}")


