*** Settings ***
Documentation   As a user
...
...     In order to benefit from all the features that the application provides,
...
...     I want to be able to easily register within the system
...
Resource    ../pages/error_page.robot
Resource    ../steps/environment_steps.robot
Resource    ../steps/landing_page_steps.robot
Resource    ../steps/login_page_steps.robot
Resource    ../steps/error_page_steps.robot
Resource    ../steps/user_information_page_steps.robot

Force Tags    regression    registration   positive login

Test Setup     Given the application is running inside the browser
Test Teardown   Close Browser
Test Template   Authentication process should success

*** Test Cases ***                              USER_NAME      PASSWORD    USR_NAME      FNAME       LNAME     PHONE
Login With Register User Name and Password      Test_User15    Test1234!   Test_User15   Manjunath   Golla     +919949632461
Login With Register User Name and Password      Test_User16    Test1234!   Test_User16   Gilbert     Edison    251-542-3622




*** Keywords ***

Authentication process should success
    [Arguments]      ${username}      ${pass}     ${uname}    ${fname}     ${lname}    ${phone}
    [Tags]  regression
    When Home page should be properly opened
    Then Navigate to Login page
    And Attempt to login by using "${username}" as username and "${pass}" as password
    Then I check user information "${uname}" and "${fname}" and "${lname}" and "${phone}" User Information screen
    When I Logout from the application
    Then I expect to be properly redirected to Home Page screen
