*** Settings ***
Documentation  This abstraction is responsible with exposing generic Register Page Steps to the test implementations

Resource    ../pages/register_page.robot
#Resource    ../test_data/accounts.robot

*** Keywords ***

Registration page should be open
    Location Should Be    ${REGISTER_URL}
    Title Should Be       ${REGISTER_PAGE_TITLE}
    Element Should Contain    ${REGISTER_PAGE_HEADER}   Register

I try to register with "${usrname}" and "${pswrd}" and "${fname}" and "${lname}" and "${phno}" a Valid account
#    ${d} =    get time
    Define "${usrname}" as Username
    Define "${pswrd}" as Password
    Define "${fname}" as First Name
    Define "${lname}" as Last Name
    Define "${phno}" as Phone Number
    Then Click on Register button

Define "${usrname}" as Username
    Input Text    ${REGISTER_USERNAME_INPUT}    ${usrname}

Define "${pswrd}" as Password
    Input Text    ${REGISTER_PASSWORD_INPUT}    ${pswrd}

Define "${fname}" as First Name
    Input Text    ${REGISTER_F_NAME_INPUT}    ${fname}

Define "${lname}" as Last Name
    Input Text    ${REGISTER_L_NAME_INPUT}    ${lname}

Define "${phno}" as Phone Number
    Input Text    ${REGISTER_PHONE_INPUT}    ${phno}

Click on Register button
    Click Button    ${REGISTER_SUBMIT_BUTTON}

I Should Have User Exists "{expected}" Message
    Element Text Should Be  ${REGISTERED_EXIST_MESSAGE}   ${expected}


And I try to register a Valid account
#    ${d} =    get time
    Define "${VALID_USER_USERNAME}" as Username
    Define "${VALID_USER_PASS}" as Password
    Define "${VALID_USER_FIRST_NAME}" as First Name
    Define "${VALID_USER_LAST_NAME}" as Last Name
    Define "${VALID_USER_PHONE}" as Phone Number
    Then Click on Register button



