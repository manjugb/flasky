#!/usr/bin/python
import sys
import json
import os

test_data_directory = os.path.dirname(os.path.dirname(__file__))
sys.path.append(test_data_directory)

account_data_file = os.path.join(test_data_directory, 'test_data/pay_load_data.json')

with open(account_data_file, 'r') as file:
    json_data_file = json.load(file)


def get_user_attribute_value(user_info, attribute_value):
    return json_data_file[user_info][attribute_value]


def get_update_firstname_value(user_info):
    return get_update_firstname_value(user_info, 'firstname')


def get_update_lastname(user_info):
    return get_update_lastname(user_info, 'lastname')


def get_update_phone(user_info):
    return get_update_phone(user_info, 'phone')
