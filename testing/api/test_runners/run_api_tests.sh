#!/bin/sh
rm -r ../test_results/* ;
python3 -m coverage run -m pytest ../tests/test_login.py --html=../test_results/test_login_report.html --self-contained-html
python3 -m coverage run -m pytest  ../tests/test_user_search.py  --html=../test_results/test_user_search_report.html --self-contained-html
python3 -m coverage run -m pytest ../tests/test_user_put.py --html=../test_results/test_user_api_report.html --self-contained-html
#coverage report -m
#coverage run  html=../test_results
