FROM alpine:latest
COPY . /app
WORKDIR /app
RUN ls -la /
RUN apk add --no-cache sqlite py3-pip
RUN pip3 install -r requirements.txt
ENV FLASK_PORT 8181
ENV FLASK_APP demo_app
CMD ["sh", "run.sh"]

COPY testing/ui/config/ /app/tests/config/
COPY testing/ui/pages/ /app/tests/pages/
COPY testing/ui/steps/ /app/tests/steps/
COPY testing/ui/test_data/ /app/tests/test_data/
COPY testing/ui/tests/ /app/tests/tests/
COPY testing/ui/test_suites/ /app/tests/test_suites/

RUN ls -la /
WORKDIR /app/tests/test_suites/
CMD ["sh","run_ui_negative_tests.sh"]
#CMD ["sh","/app/tests/test_suites/run_ui_negative_tests.sh"]