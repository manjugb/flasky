# pylint: disable=redefined-outer-name
import pytest

from demo_app import api as flask_api


@pytest.fixture
def app():
    yield flask_api


@pytest.fixture
def client(app):
    return api.test_client()