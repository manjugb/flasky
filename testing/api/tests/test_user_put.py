import json
import unittest

import requests

from testing.api.config.endpoints import Endpoints
from testing.api.support import account_data_provider, payload_data_provider
from testing.api.tests.api_test_client import get_formatted_endpoint_url, get_token, getUserResponse


class TestUserPut(unittest.TestCase):
    def test_user_put_success(self):
        try:
            user='EXISTING_USER_A'

            #print(token)
            payload = {'firstname': 'Manjunath', 'lastname': 'Golla Bala', 'phone': '4656666612'}
            #data = json.dumps(payload)

            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }

            #s = requests.Session()
            response = requests.put(get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),data=json.dumps(payload),headers=myheaders)
            #response = requests.put('http://0.0.0.0:5000/api' + '/' + Endpoints.USERS + '/' + account_data_provider.get_account_username(user),headers=headers,data=json.dumps(payload))
            #print(response.json())
            print(response.url)
            print(response.text)
            print(response.json())
            self.assertEqual(201, response.status_code)
            self.assertTrue(response.ok)
            #print(response.content)

            """ Verify updated information should not be equal from json file to database"""
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200,res.status_code)

            self.assertNotEqual(account_data_provider.get_account_first_name(user),
                             res.json()['payload']['firstname'])
            self.assertNotEqual(account_data_provider.get_account_last_name(user),
                             res.json()['payload']['lastname'])
            self.assertNotEqual(account_data_provider.get_account_phone(user),
                             res.json()['payload']['phone'])
            print(res.status_code)
            print(res.json())
            self.assertTrue(response.ok)
            self.assertEqual(200,res.status_code)

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_put_not_exist_user(self):
        """ this test validates invalid user  can try to update with out token"""
        try:
            user="NOT_EXIST_USER"
            myheaders = {'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            payload = {'firstname': 'Doe', 'lastname': 'Jane', 'phone': '343256465'}
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)

            print(response.json())
            print(response.status_code)
            self.assertEqual(response.status_code,401)
            self.assertFalse(response.ok)
            #verify updated information



        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_update_incomplete_json(self):
        """ this test validtes the json using PUT Request
        input two fields valid one is empty and two values should be
        updated """

        try:
            user='EXISTING_USER_A'
            payload = {'firstname': 'James','lastname': 'Bond','phone': ''}
            myheaders = {'Token': '{}'.format(get_token()),
                     'Content-Type': 'application/json',
                     'Accept': 'application/json',
                     }
            response = requests.put(
            get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
            data=json.dumps(payload), headers=myheaders)
            print(response.json())
            print(response.status_code)
            self.assertEqual(201,response.status_code)
            self.assertTrue(response.ok)
            """ Verify updated information """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            print(res.status_code)
            print(res.json())

            """ Verify updated information should not be equal from json file to database"""
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)

            self.assertNotEqual(account_data_provider.get_account_first_name(user),
                                res.json()['payload']['firstname'])
            self.assertNotEqual(account_data_provider.get_account_last_name(user),
                                res.json()['payload']['lastname'])
            self.assertNotEqual(account_data_provider.get_account_phone(user),
                                res.json()['payload']['phone'])

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_put_user_last_name_empty(self):
        """ this test validates two values with empty values """
        try:
            user = 'EXISTING_USER_A'
            #user_info = 'USER_INFO_A'
            #print(payload_data_provider.get_update_lastname(user_info))
            payload = {'firstname': 'Manjunath','lastname':'','phone':'1212611113'}
            myheaders = {'Token': '{}'.format(get_token()),
                     'Content-Type': 'application/json',
                     'Accept': 'application/json',
                     }
            response = requests.put(
            get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
            data=json.dumps(payload), headers=myheaders)
            print(response.json())
            print(response.status_code)
            self.assertEqual(201, response.status_code)
            self.assertEqual('SUCCESS', response.json()['status'])
            #self.assertEqual(52, len(response.json()['token']))
            self.assertTrue(response.ok)
            """ Verify updated information """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            print(res.status_code)
            print(res.json())

            """ Verify updated information should not be equal from json file to database"""
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)

            self.assertNotEqual(account_data_provider.get_account_first_name(user),
                                res.json()['payload']['firstname'])
            self.assertNotEqual(account_data_provider.get_account_last_name(user),
                                res.json()['payload']['lastname'])
            self.assertNotEqual(account_data_provider.get_account_phone(user),
                                res.json()['payload']['phone'])

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_put_empty_firstname(self):
        """ this test validates valid two values with
        empty firstname """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname': '', 'lastname': '', 'phone': '1212611113'}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            print(response.json())
            print(response.status_code)
            self.assertEqual(201, response.status_code)
            self.assertEqual('SUCCESS', response.json()['status'])
            #self.assertEqual(52, len(response.json()['token']))
            self.assertTrue(response.ok)
            """ Verify updated information """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            print(res.status_code)
            print(res.json())
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_put_all_empty(self):
        """ this test validates put all are empty
        """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname': '', 'lastname': '', 'phone': '1212611113'}
            myheaders = {'Token': '{}'.format(get_token()),
                     'Content-Type': 'application/json',
                     'Accept': 'application/json',
                     }
            response = requests.put(
            get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
            data=json.dumps(payload), headers=myheaders)
            #print(response.status_code)
            #print(response.json())
            self.assertEqual(201,response.status_code)
            self.assertEqual('SUCCESS', response.json()['status'])
            #self.assertEqual(52, len(response.json()['token']))

            """ Verify updated information should not be equal from json file to database after updation """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)

            self.assertNotEqual(account_data_provider.get_account_first_name(user),
                                res.json()['payload']['firstname'])
            self.assertNotEqual(account_data_provider.get_account_last_name(user),
                                res.json()['payload']['lastname'])
            self.assertNotEqual(account_data_provider.get_account_phone(user),
                                res.json()['payload']['phone'])

        except requests.exceptions.HTTPError as errh:
            print(errh)

        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_put_single_field_firstname(self):
        """ this test validates weather PUT Request send partially """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname': 'Tom'}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual('SUCCESS', response.json()['status'])
            #self.assertEqual(52, len(response.json()['token']))
            """ Verify updated information should not be equal from json file to database after updation """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)

            self.assertNotEqual(account_data_provider.get_account_first_name(user),
                                res.json()['payload']['firstname'])

            print(response.status_code)
            print(response.json())

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_put_partial_lastname(self):
        """ this test validate lastname whether able to accept or not """
        try:
            user = 'EXISTING_USER_A'
            payload = {'lastname': 'Tom'}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(201,response.status_code)
            self.assertEqual('SUCCESS', response.json()['status'])
            #self.assertEqual(52, len(response.json()['token']))
            print(response.json())
            """ Verify updated information should not be equal from json file to database after updation """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)
            print(res.json())
            if account_data_provider.get_account_username(user) == res.json()['payload']['lastname']:
                self.assertEqual(account_data_provider.get_account_username(user),
                                 res.json()['payload']['lastname'])
            else:
                self.assertNotEqual(account_data_provider.get_account_first_name(user),
                                res.json()['payload']['lastname'])

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_put_partial_phone(self):
        """ this test validates wether phone number """
        try:
            user = 'EXISTING_USER_A'
            payload = {'phone': '789+66+61164f'}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(201, response.status_code)
            print(response.json())
            self.assertEqual('SUCCESS', response.json()['status'])
            #print(myheaders.update())
            #print(len(response.json()['token']))
            #self.assertEqual(52, len(response.json()['token']))
            """ Verify updated information should not be equal from json file to database after updation """
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)
            print(res.json())
            if account_data_provider.get_account_username(user) == res.json()['payload']['phone']:
                self.assertEqual(account_data_provider.get_account_username(user),
                                 res.json()['payload']['phone'])
            else:
                self.assertNotEqual(account_data_provider.get_account_first_name(user),
                                    res.json()['payload']['lastname'])
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_put_payload_empty(self):
        """ this test validates wether able to trigger PUT Request
        with out payload attributes should process give 500 error """
        try:
            user = 'EXISTING_USER_A'
            payload = {}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(201, response.status_code)
            print(response.json())
            self.assertEqual('SUCCESS', response.json()['status'])
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)
            print(res.json())

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_payload_invalid_file(self):
        """ this test validates the invalid payload not to trigger
        give valid error message not to autherized to put """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname':'Micheal','familyname':'gilber','phone':'56565665'}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(403, response.status_code)
            print(response.json())
            self.assertEqual('FAILURE', response.json()['status'])
            self.assertEqual('Field update not allowed',response.json()['message'])

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_phone_remove_quotation(self):
        """ this test validates input as with out single quotation
        payload """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname': 'Micheal', 'lastname': 'gilbert', 'phone': 56565665}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(201, response.status_code)
            print(response.text)

            self.assertEqual('SUCCESS', response.json()['status'])
            self.assertEqual('Updated', response.json()['message'])
            #verify that after updation
            res = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertEqual(200, res.status_code)
            print(res.json())

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_user_rem_quoation_first(self):
        """this test validates first name and last name input as with out single
        quotation should give Name Error """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname': Micheal, 'lastname': 'gilbert', 'phone': 56565665}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(201, response.status_code)
            print(response.text)

        except NameError as nameer:
            print(nameer)

    def test_user_rem_quote_lastname(self):
        """ this test validates firstname value with out quotation """
        try:
            user = 'EXISTING_USER_A'
            payload = {'firstname': 'Micheal', 'lastname': gilbert, 'phone': 56565665}
            myheaders = {'Token': '{}'.format(get_token()),
                         'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         }
            response = requests.put(
                get_formatted_endpoint_url(Endpoints.USERS + '/' + account_data_provider.get_account_username(user)),
                data=json.dumps(payload), headers=myheaders)
            self.assertEqual(201, response.status_code)
            print(response.text)

        except NameError as nameer:
            print(nameer)





