
*** Settings ***
Documentation  Positive Authentication
...     As an Admin
...
...     In order to limit the access of unauthorised accounts inside of the application
...
...     I want to check if valid login data are accepted by any mean
...
Resource    ../pages/error_page.robot
Resource    ../steps/environment_steps.robot
Resource    ../steps/landing_page_steps.robot
Resource    ../steps/login_page_steps.robot
Resource    ../steps/error_page_steps.robot
Resource    ../steps/user_information_page_steps.robot

Force Tags    regression    registration   positive

Test Setup     Given the application is running inside the browser
Test Teardown   Close Browser
Test Template    Aleardy Registration should not be processed again



*** Test Cases ***                       USER_NAME      PASSWORD      FNAME       LNAME     PHONE           MESSAGE
Registation already existing values      Test_User16     Test1234!    Enstien     Lingaraj  215-365-8512    User Test_User16 is already registered.


*** Keywords ***
Aleardy Registration should not be processed again
    [Arguments]      ${usrname}     ${pswrd}    ${fname}     ${lname}    ${phno}   ${expected_msg}
    [Tags]  regression positive negative login
    When Home page should be properly opened
    Then Navigate to Registration page
    And I try to register with "${usrname}" and "${pswrd}" and "${fname}" and "${lname}" and "${phno}" a Valid account
    Then I expect to be properly redirected to Login Page screen
    And Attempt to login by using "${usrname}" as username and "${pswrd}" as password
    Then I check user information "${usrname}" and "${fname}" and "${lname}" and "${phno}" User Information screen
    When I Logout from the application
    Then I expect to be properly redirected to Home Page screen
    When Home page should be properly opened
    Then Navigate to Registration page
    And I try to register with "${usrname}" and "${pswrd}" and "${fname}" and "${lname}" and "${phno}" a Valid account
    And Click on Register button
    And I Should Have User Exists "{expected}" Message
    Then Registration page should be open


