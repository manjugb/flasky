class Endpoints:
    AUTHENTICATION = "auth/token"
    USERS = "users"
    LOGIN = "login"
    REGISTER = "register"
