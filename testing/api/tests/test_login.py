import json
import unittest

import requests
import urllib3

from demo_app.api import put_specific_user
from testing.api.config.endpoints import Endpoints
from testing.api.support import account_data_provider
from testing.api.tests.api_test_client import getUserResponse, EXISTING_USER_ENDPOINT, NON_EXISTING_USER_ENDPOINT, \
    expectedUserData, invalidData, \
    NON_EXISTING_USER_ENDPOINT1, get_formatted_endpoint_login_url, expectedUserData1, get_token, perform_token_call, \
    get_formatted_endpoint_url

response_invalid_01 = NON_EXISTING_USER_ENDPOINT1
#this creates create token and login valid credentials
response_valid = getUserResponse(EXISTING_USER_ENDPOINT)




#this calls create token,login feature from api.py

response_valid_login= requests.get(get_formatted_endpoint_login_url(Endpoints.LOGIN),expectedUserData)
#response_valid_login= requests.get(get_formatted_endpoint_login_url(Endpoints.LOGIN),expectedUserData1)
#
# import logging
# logger = logging.getLogger('global-log')


class LoginUnitTest(unittest.TestCase):

    def test_login_success(self):
        """ Future test: this test validates
        user able to login """
        #logger.info("Validation of Login with Existing User Started")
        try:
            if EXISTING_USER_ENDPOINT:

              # logger.info(token)
                #request_login = requests.get(get_formatted_endpoint_login_url(Endpoints.LOGIN),expectedUserData)
                print(response_valid_login.url)
                print(response_valid_login.text)
               # print(response_valid_login.json()[])
                print(response_valid_login.links)
                self.assertEqual(response_valid_login.status_code,200)
                self.assertTrue(response_valid_login.ok)
                print(response_valid_login.url)

            else:
                self.assertNotEqual(response_valid.status_code, 200)
                self.assertEqual(response_valid_login.status_code, 200)
                self.assertFalse(response_valid_login.ok)
                self.assertNotEqual(response_valid_login.status_code, 500)
            #logger.info("Validation of Login with existing user ended")

        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def test_login_failure(self):
        """ Future this test validates user authentication for
         Invalid Data """

        try:
            if NON_EXISTING_USER_ENDPOINT:

                #self.assertTrue(token)
                self.assertEqual(response_valid_login.status_code,200)
                self.assertTrue(response_valid_login.ok)
                self.assertTrue(response_valid_login.url)
                print(response_valid_login.status_code)
                #print(response_invalid.status_code)
                print(response_valid.url)
                #print(response_invalid.json())
            else:
                self.assertNotEqual(response_valid_login.status_code,200)
                self.assertTrue(response_valid_login.ok)
                #print()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

