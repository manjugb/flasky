*** Settings ***
Documentation   As a user
...
...     In order to benefit from all the features that the application provides,
...
...     I want to be able to easily register within the system
...
Resource    ../pages/error_page.robot
Resource    ../steps/environment_steps.robot
Resource    ../steps/landing_page_steps.robot
Resource    ../steps/login_page_steps.robot
Resource    ../steps/error_page_steps.robot
Resource    ../steps/user_information_page_steps.robot

Force Tags     registation

Test Setup     Given the application is running inside the browser
Test Teardown   Close Browser
Test Template   Authentication process should success

*** Test Cases ***                              USER_NAME      PASSWORD    USR_NAME      FNAME       LNAME     PHONE
Login With Register User Name and Password      api_user       Test1234!   api_user      API         USER      215-365-4512
Login With Register User Name and Password      ui_user        Test@1234!  ui_user       Tester      user      254-365-2541



*** Keywords ***

Authentication process should success
    [Arguments]      ${username}      ${pass}     ${uname}    ${fname}     ${lname}    ${phone}
    [Tags]  regression
    When Home page should be properly opened
    Then Navigate to Login page
    And Attempt to login by using "${username}" as username and "${pass}" as password
    Then I check user information "${uname}" and "${fname}" and "${lname}" and "${phone}" User Information screen
    When I Logout from the application
    Then I expect to be properly redirected to Home Page screen
