
import unittest

import requests

from testing.api.config.endpoints import Endpoints
from testing.api.support import account_data_provider
from testing.api.tests.api_test_client import expectedUserData, getUserResponse, EXISTING_USER_ENDPOINT, get_token, \
    perform_token_call


class TestUser(unittest.TestCase):

    def test_token_endpoint(self):
        """ this test validates token"""
        response = perform_token_call()
        self.assertEqual("application/json", response.headers['Content-Type'])
        self.assertTrue(response.ok)
        self.assertTrue(200, response.status_code)
        self.assertEqual('SUCCESS', response.json()['status'])
        self.assertEqual(52, len(response.json()['token']))
        if response.json()['token'].isalnum():
            self.assertTrue(response.json()['token'].isalnum())
        else:
            self.assertFalse(response.json()['token'].isalnum())
        print(response.text)

    # def test_responseHeader(self):
    #     """ this test validate response header"""
    #     try:
    #         response = getUserResponse(EXISTING_USER_ENDPOINT)
    #         #token = get_token()
    #         #self.assertTrue(token)
    #         #print(f"Token From Response Header : {token}")
    #         print(response.url)
    #         self.assertEqual('application/json', response.headers['Content-Type'])
    #         self.assertEqual('Werkzeug/1.0.1 Python/3.7.5', response.headers['Server'])
    #         self.assertEqual(124, int(response.headers['Content-Length']))
    #         # print(response.json)
    #     except requests.exceptions.HTTPError as errh:
    #         print(f"Http Error: {errh}")
    #     except requests.exceptions.ConnectionError as errc:
    #         print(f"Http Connection Error: {errc}")
    #     except requests.exceptions.Timeout as errt:
    #         print(f"Time Out: {errt}")
    #     except requests.exceptions.RequestException as err:
    #         print(f"Request Exception: {err}")
    #         print(response.raise_for_status())

    def test_get_users_success(self):
        """ this test case validates to list of users"""
        try:
            response = getUserResponse(Endpoints.USERS)

            self.assertTrue(response.ok)
            self.assertEqual(response.status_code,200)
            print(response.text)
            print(response.url)
            users = response.json()['payload']
            expected_username = account_data_provider.get_account_username(expectedUserData)
            if expected_username not in users:
               self.fail(expected_username + ' could not be found within the users list!')

        except requests.exceptions.HTTPError as error_http:
            print(error_http)
        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)
            print(response.raise_for_status())

    def test_get_users_failure_404(self):
        """ this test case validates to not to list the
        users and get 404 status code"""
        try:
            response = getUserResponse(Endpoints.USERS + '/')

            self.assertFalse(response.ok)
            self.assertEqual(response.status_code,404)
            print(response.text)
           # users = response.json()['payload']
           # expected_username = account_data_provider.get_account_username(expectedUserData)
           # if expected_username not in users:
            #   self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(error_http)
        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)
            print(response.raise_for_status())

    def test_get_ussers_status_code_500(self):
        """this test validates invalid end point and gives that 500 error"""
        """ this test case validates to list """
        try:
            response = getUserResponse(Endpoints.USERS + '/userlist')
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code, 500)
            print(response.text)
            # users = response.json()['payload']
            # expected_username = account_data_provider.get_account_username(expectedUserData)
            # if expected_username not in users:
            #     self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(f"HTTP Error:{error_http}")
        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)
            print(response.raise_for_status())

    def test_get_users_status_code_404(self):
        """ this test case validates to list the users  """
        try:
            response = getUserResponse(Endpoints.USERS + Endpoints.LOGIN)
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code, 404)
            print(response.text)
            #users = response.json()['payload']
            # expected_username = account_data_provider.get_account_username(expectedUserData)
            # if expected_username not in users:
            #     self.fail(expected_username + ' could not be found within the users list!')
        except requests.exceptions.HTTPError as error_http:
            print(error_http)
        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)
            print(response.raise_for_status())

    def test_get_user_success_200(self):
        """ this test validates able to get single user
        verify user information """
        try:
           user='EXISTING_USER_A'
           response = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
           self.assertTrue(response.ok)
           self.assertEqual(response.status_code, 200)
           print(response.text)
           #print(response.url)
           #print(response.history)
           self.assertEqual(account_data_provider.get_account_first_name(user),
                            response.json()['payload']['firstname'])
           self.assertEqual(account_data_provider.get_account_last_name(user),
                            response.json()['payload']['lastname'])
           self.assertEqual(account_data_provider.get_account_phone(user),
                            response.json()['payload']['phone'])

        except requests.exceptions.HTTPError as error_http:
            print(error_http)
        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)
            print(response.raise_for_status())

    def test_get_user_failure_500(self):
        """ this test validates user which is not exist
        and gives valid error message and check runtime exceptions """
        try:
            user = 'NOT_EXIST_USER'
            #exp_url = Endpoints.USERS + '/' + account_data_provider.get_account_username(user)
            exp_url = 'http://0.0.0.0:5000/api/users/api_user3'
            response = getUserResponse(Endpoints.USERS + '/' + account_data_provider.get_account_username(user))
            self.assertFalse(response.ok)
            self.assertEqual(response.status_code, 500)
            self.assertEqual(response.url,exp_url)
            #print(response.url)

        except requests.exceptions.HTTPError as error_http:
            print(error_http)
        except requests.exceptions.ConnectionError as error_connect:
            print(error_connect)
        except requests.exceptions.Timeout as error_timeout:
            print(error_timeout)
        except requests.exceptions.RequestException as error:
            print(error)


