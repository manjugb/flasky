# CRF Assignment Implementation #

## whats need ##

### Chromedriver
1. Install _[Chromedriver](https://chromedriver.chromium.org/) based on operating system

2. Follow the instructions until _chromedriver_ installing success

### Python
* Install Python Binding Based on Your Operating System https://www.python.org/downloads


## Install Docker Image ##

* Install Docker go to https://docs.docker.com/engine/install/ and follow the instructions based on operating systems
* once successfull check docker run hello-world if this is runs, docker setup successfull
* install docker-compose https://docs.docker.com/compose/install/

### Run on Docker Image ##
* docker-compose build and this install image code,requirements and create an image
* docker compose up --no-start this wont start just create containers 
* docker compose -d up


## Test Execution ##

#### API Tests ####
* Please change PORT no in testing/api/config/environment_contant.py Based on  local settings
* In order to execute the UI related tests (as a test suite), you need to navigate from the `terminal` to `testing/api/test_runner`
* To execute any of the desired shell file that is pointing to an associated test suite type, run `sh <your_desired_suite>` command


#### UI Tests ####

* Please change url port no in testing/ui/config in config.robot based on local settings
* In order to execute the UI related tests (as a test suite), you need to navigate from the `terminal` to `testing/ui/test_suites`
* To execute any of the desired shell file that is pointing to an associated test suite type, run `sh <your_desired_suite>` command

## Test Report ##

#### Test Reports ####
* In order to see the UI Report goto the folder 'testing/ui/test_results"
* In order to see the API Test Reports go to the folder 'testing/api/test_reults'

## Jenkins Execution ##
* sudo apt-get update
* sudo apt-get install -y xvfb
#### setup command line below ####
Xvfb -ac :99 -screen 0 1280x1024x16 & export DISPLAY=:99
## configure in jenkins pipeline ##
* https://www.jenkins.io/doc/pipeline/steps/xvfb/
* https://plugins.jenkins.io/xvfb/



## Jenkins Excecution Shell Setup ##
#### UI Testing ####
#### Choose Executive Shell from the build and write below instructions in executive shell ####
* #!/bin/bash
* export DISPLAY=:99
* echo "checking robot version"
* python3 -m robot --version
* echo "Started Negative Test Suite"
* cd test_suites/
* sh "run_ui_negative_tests.sh"
* echo "Ended Negative Test Suite"
* echo "Start Regression Test Suite"
* sh "run_ui_regression_tests.sh"
* echo "Ended Regression Test Suite"
* echo "Start smoke Test Suite"
* sh "run_ui_smoke_tests.sh"
* echo "Ended Smoke Tests"
* cd ..
* cd ..
#### API Testing ####
echo "/**** API Tests ****/"
* cd api/test_runners/
* sh "run_api_tests.sh"









