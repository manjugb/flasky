
BASE_HOST = "http://0.0.0.0"
PORT = 5000
BASE_PATH = "/api/"
BASE_PATH_LOGIN = "/"


def get_formatted_URL():
    return "{}:{}{}".format(BASE_HOST, PORT, BASE_PATH)

def get_formatted_URL_login():
    return "{}:{}{}".format(BASE_HOST, PORT, BASE_PATH_LOGIN)
